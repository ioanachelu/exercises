var app = angular.module('App');

app.directive('myCustomer', function() {
	
	var directive = {
		// scope: true,
		// scope: {

		// },
		scope: {
			customers: '='
		},
		templateUrl: 'app/directives/customerTemplate.html'
	};
	
	return directive;
});