describe('example test', function() {
    it('should be true', function() {
        expect(true).toBe(true);
    });
});

describe("provider test", function(){

    beforeEach(module("App"));

    var productService, productFactory, customer;

    beforeEach(inject(function(_productService_, _productFactory_, _customer_){
       productService = _productService_;
       productFactory = _productFactory_;
       customer = _customer_;
    }));

    it("should be defined", function(){
        expect(productService.getProduct).toBeDefined();
        expect(productFactory.getProduct).toBeDefined();
        expect(customer.customerName).toBeDefined();
    });

    it("should retrun chair", function(){
        expect(productService.getProduct()).toBe('chair');
        expect(productFactory.getProduct()).toBe('table');
        expect(customer.customerName).toBe('John Doe');
    });
});