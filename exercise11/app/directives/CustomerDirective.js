var app = angular.module('App');

app.directive('myCustomer', function(customerService) {
	
	var directive = {
		scope: {
			customers: '='
		},
		templateUrl: 'app/directives/customerTemplate.html',
		link: function(scope, element, attrs) {
			scope.update = function(customer) {
				console.log(customer);
				customerService.updateCustomer(customer).then(function(data) {
					console.log(data);
				});
			}
		}
	};

	return directive;

});