var app = angular.module('App');

app.controller('CustomerController', function($scope, customer, productService, productFactory, customerService) {
	$scope.customer = {};
	
	customerService.getCustomers().then(function(data) {
		$scope.customers = data.data;
	});

	$scope.createCustomer = function() {
		customerService.createCustomer($scope.customer).then(function(data) {
			$scope.customers = data.data;
		});
		$scope.customer = {};
	};
	
});