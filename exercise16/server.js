var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

mongoose.connect('localhost:27017/testDB');

app.use(express.static(__dirname + '/'));                 
app.use(bodyParser.urlencoded({'extended':'true'}));      
app.use(bodyParser.json());                               
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(methodOverride());

var products = ['spoon', 'knife', 'mug', 'plate', 'fork', 'bowl'];

// define model =================
var Customer = mongoose.model('Customer', {
    name : String,
    address: String,
    email: String,
    products: [String]
});

// get all customers
app.get('/api/customer', function(req, res) {
    Customer.find(function(err, customers) {
        if (err)
            res.send(err)
        res.json(customers);
    });
});

// get customer by id
app.get('/api/customer/:customer_id', function(req, res) {
    Customer.findOne({
            _id:  req.params.customer_id
        },
        function(err, customer) {
            if (err)
                res.send(err)
            res.json(customer);
        }
    );
});

// return a products for customer by id
app.post('/api/customer/products', function(req, res) {
    Customer.findOne({
            _id:  req.body.id
        },
        function(err, customer) {
            if (err)
                res.send(err)
            var customerProducts = [];
            for(var i = 0; i < customer.products.length; i++) {
                customerProducts.push(products[i]);
            }
            setTimeout((function() {
                res.json(customerProducts);
            }), 4000);
        }
    );
});

// create customer
app.post('/api/customer', function(req, res) {
    var cutomerProducts = [];
    for (var i = 0; i < 2; i++) {
        cutomerProducts.push(Math.floor(Math.random() * (products.length + 1)));
    };
    Customer.create({
        name : req.body.name,
        address: req.body.address,
        email: req.body.email,
        products: cutomerProducts
    }, function(err, customer) {
        if (err)
            res.send(err);
        Customer.find(function(err, customers) {
            if (err)
                res.send(err)
            res.json(customers);
        });
    });
});

// update customer
app.put('/api/customer', function(req, res) {
    Customer.update({
     _id: req.body._id 
    },{
        name : req.body.name,
        address: req.body.address,
        email: req.body.email
    }, function(err, customer) {
        if (err)
            res.send(err);
        Customer.find(function(err, customers) {
            if (err)
                res.send(err)
            res.json(customers);
        });
    });
});

// delete customer
app.delete('/api/customer/:customer_id', function(req, res) {
    Customer.remove({
        _id : req.params.customer_id
    }, function(err, customer) {
        if (err)
            res.send(err);
        Customer.find(function(err, customers) {
            if (err)
                res.send(err)
            res.json(customers);
        });
    });
});

app.get('*', function(req, res) {
    res.sendfile('index.html'); // load the single view file (angular will handle the page changes on the front-end)
});

// listen (start app with node server.js) ======================================
app.listen(8080);
console.log("App listening on port 8080");