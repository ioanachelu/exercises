var app = angular.module('App');

app.directive('myCustomer', function(customerService) {
	var directive = {
		transclude: true,
		templateUrl: 'app/directives/customerTemplate.html',
		link: function(scope, element, attrs) {
			scope.update = function(customer) {
				customerService.updateCustomer(customer).then(function(data) {
					console.log(data);
				});
			};
		}
	};
	return directive;
});