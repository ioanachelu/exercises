var app = angular.module('App');

app.controller('SingleCustomerController', function($scope, $routeParams, customerService, $q) {
    $scope.customer = {};
    $scope.products = {};
    $scope.isLoading = true;

    var customer = customerService.getCustomer($routeParams.id);
    var products = customerService.getCustomerProducts({id: $routeParams.id});

    $q.all([customer, products]).then(function(data){
        $scope.customer = data[0].data;
        $scope.products = data[1].data;
        $scope.isLoading = false;
    });
});