var app = angular.module('App', ['ui.bootstrap']);

app.controller('CustomerController', function($scope) {
	$scope.customer = {};

	$scope.createCustomer = function() {
		console.log('Customer ' + $scope.customer.name + ' was submitted!');
	};
});