var app = angular.module('App');

app.directive('myCustomer', function(orderService) {
	
	var directive = {
		scope: {
			customers: '='
		},
		templateUrl: 'app/directives/customerTemplate.html',
		controller: function($scope, $element, $attrs) {
			// controller code here
			orderService.addFunction('controller');
		},
		compile: function(element, attributes) {
			// compile code here
			orderService.addFunction('compile');

			return {

				pre: function(scope, element, attributes, controller) {
					// pre-link code here
					orderService.addFunction('pre-link');
				},
				post: function(scope, element, attributes, controller) {
					// post-link code here
					scope.order = orderService.addFunction('post-link');
				}

			}

		}
	};

	return directive;

});