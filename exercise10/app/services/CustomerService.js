var app = angular.module('App');

app.provider('customer', function() {
	var name;
	return {
		setName: function(value) {
			name = value;
		},
		$get: function() {
			return {
				customerName: name
			}
		}
	}
});

app.config(function(customerProvider) {
	customerProvider.setName("John Doe");
});

app.service('productService', function() {
	var _product = 'chair';
	this.getProduct = function() {
		return _product;
	}
});

app.factory('productFactory', function() {
	var productName = 'table';
	var service = {};
	service.getProduct = function() {
		return productName;
	}
	return service;
});

app.factory('customerService', function($http) {
	var service = {
		getCustomers: function() {
			return $http.get('/api/customer');
		},
		createCustomer: function(customer) {
			return $http.post('/api/customer', customer)
		}
	}
	return service;
});

app.service('orderService', function() {
	var functions = [];

	this.addFunction = function(fnc) {
		functions.push(fnc);
		return functions;
	}
});