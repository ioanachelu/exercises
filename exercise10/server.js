var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

mongoose.connect('localhost:27017/testDB');

app.use(express.static(__dirname + '/'));                 
app.use(bodyParser.urlencoded({'extended':'true'}));      
app.use(bodyParser.json());                               
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(methodOverride());

// define model =================
var Customer = mongoose.model('Customer', {
    name : String,
    address: String,
    email: String
});

// get all customers
app.get('/api/customer', function(req, res) {
    Customer.find(function(err, customers) {
        if (err)
            res.send(err)
        res.json(customers);
    });
});

// create customer
app.post('/api/customer', function(req, res) {
    Customer.create({
        name : req.body.name,
        address: req.body.address,
        email: req.body.email
    }, function(err, customer) {
        if (err)
            res.send(err);
        Customer.find(function(err, customers) {
            if (err)
                res.send(err)
            res.json(customers);
        });
    });
});

app.get('*', function(req, res) {
    res.sendfile('index.html'); // load the single view file (angular will handle the page changes on the front-end)
});

// listen (start app with node server.js) ======================================
app.listen(8080);
console.log("App listening on port 8080");