// app.spec.js
describe('AngularJS App', function() {

    var customers = element.all(by.repeater('customer in customers'));
    var submit = element(by.buttonText('Create Customer'));
    var nameField = element(by.model('customer.name'));
    var addressField = element(by.model('customer.address'));
    var emailField = element(by.model('customer.email'));

    beforeEach(function(){
        browser.get('http://localhost:8080/');
    });

    it('should have a title', function() {
        expect(browser.getTitle()).toEqual('AngularJS');
    });

    it('should add a customer', function() {
        var initialCount = customers.count();

        nameField.sendKeys('Jim Doe');
        addressField.sendKeys('some random address');
        emailField.sendKeys('jimdoe@email.com');

        submit.click().then(function() {
            expect(customers.count()).toBeGreaterThan(initialCount);
        });
    });

    it('should delete last customer', function() {
        var initialCount = customers.count();
        var lastCustomer = customers.last();
        var deleteButton = lastCustomer.element(by.css('.btn-danger'));

        deleteButton.click().then(function(){
            expect(initialCount).toBeGreaterThan(customers.count());
        });

    });
  
});