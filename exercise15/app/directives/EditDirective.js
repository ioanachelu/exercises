angular.module("modelEditor", [])

.directive("edit", function($timeout, $http) {
    return {
        restrict: "A",
        replace: true,
        template: '<div>' +
	        '<div ng-hide="view.editorEnabled" ng-click="enableEdit()">' +
	            '{{value}} ' +
	        '</div>' +
	        '<div ng-show="view.editorEnabled">' +
	            '<input ng-model="view.editableValue">' +
	            '<a href="#" ng-click="save()">Save</a>' +
	            ' or ' +
	            '<a ng-click="disableEdit()">cancel</a>.' +
	        '</div>' +
	    '</div>',
        scope: {
            value: "=edit",
            callback: '@'
        },
        controller: function($scope, $element, $attrs) {
            $scope.view = {
                editableValue: $scope.value,
                editorEnabled: false
            };

            $scope.enableEdit = function() {
                $scope.view.editorEnabled = true;
                $scope.view.editableValue = $scope.value;
            };

            $scope.disableEdit = function() {
                $scope.view.editorEnabled = false;
            };

            $scope.save = function() {
                $scope.value = $scope.view.editableValue;
                $scope.disableEdit();
                var waitForRender = function() {
					if($http.pendingRequests.length > 0) {
						$timeout(waitForRender);
					} else {
						$scope.$parent.$eval($attrs.callback)
					}
				}
				$timeout(waitForRender);
            };
        }
    };
});