var app = angular.module('App', 
    [
        'ngRoute',
        'ui.bootstrap',
        'modelEditor',
        'panel.module'
    ]
);

app.config(function($routeProvider) {
    $routeProvider

        .when('/', {
            templateUrl : '/app/views/customers.html',
            controller  : 'CustomerController'
        })
        .when('/customer/:id', {
            templateUrl : 'app/views/customer.html',
            controller  : 'SingleCustomerController'
        })
        .otherwise({
            redirectTo: '/'
        });

});