var app = angular.module('App');

app.provider('customer', function() {
	var name;
	return {
		setName: function(value) {
			name = value;
		},
		$get: function() {
			return {
				customerName: name
			}
		}
	}
});

app.config(function(customerProvider) {
	customerProvider.setName("John Doe");
});

app.service('productService', function() {
	var _product = 'chair';
	this.getProduct = function() {
		return _product;
	}
});

app.factory('productFactory', function() {
	var productName = 'table';
	var service = {};
	service.getProduct = function() {
		return productName;
	}
	return service;
});

app.factory('customerService', function($http) {
	var service = {
		getCustomer: function(id) {
			return $http.get('/api/customer/' + id);
		},
		getCustomers: function() {
			return $http.get('/api/customer');
		},
		createCustomer: function(customer) {
			return $http.post('/api/customer', customer)
		},
		updateCustomer: function(customer) {
			return $http.put('/api/customer', customer);
		},
		deleteCustomer: function(id) {
			return $http.delete('/api/customer/' + id);	
		}
	}
	return service;
});

app.service('orderService', function() {
	var functions = [];

	this.addFunction = function(fnc) {
		functions.push(fnc);
		return functions;
	}
});