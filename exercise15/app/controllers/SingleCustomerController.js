var app = angular.module('App');

app.controller('SingleCustomerController', function($scope, $routeParams, customerService) {

    $scope.customer = {};

    customerService.getCustomer($routeParams.id).then(function(data) {
        $scope.customer = data.data;
    });

});