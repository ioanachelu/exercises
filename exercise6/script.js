var app = angular.module('App', ['ui.bootstrap']);
app.provider('customer', function() {
	var name;
	return {
		setName: function(value) {
			name = value;
		},
		$get: function() {
			return {
				customerName: name
			}
		}
	}
});
app.config(function(customerProvider) {
	customerProvider.setName("John Doe");
});
app.service('productService', function() {
	var _product = 'chair';
	this.getProduct = function() {
		return _product;
	}
});
app.factory('productFactory', function() {
	var productName = 'table';
	var service = {};
	service.getProduct = function() {
		return productName;
	}
	return service;
});
app.controller('CustomerController', function($scope, customer, productService, productFactory) {
	$scope.customer = {};
	$scope.customerName = customer.customerName;

	$scope.productFromService = productService.getProduct();
	$scope.productFromFactory = productFactory.getProduct();

	$scope.createCustomer = function() {
		console.log('Customer ' + $scope.customer.name + ' was submitted!');
	};
});