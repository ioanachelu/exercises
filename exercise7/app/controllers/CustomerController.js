var app = angular.module('App');

app.controller('CustomerController', function($scope, customer, productService, productFactory) {
	$scope.customer = {};
	$scope.customerName = customer.customerName;

	$scope.productFromService = productService.getProduct();
	$scope.productFromFactory = productFactory.getProduct();

	$scope.createCustomer = function() {
		console.log('Customer ' + $scope.customer.name + ' was submitted!');
	};
});