var app = angular.module('App', []);

app.controller('CustomerController', function($scope) {
	$scope.customer = {};

	$scope.createCustomer = function() {
		console.log('Name: ' + $scope.customer.name);
        console.log('Address: ' + $scope.customer.address);
        console.log('Email: ' + $scope.customer.email);
	};
});