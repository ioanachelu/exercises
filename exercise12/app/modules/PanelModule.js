angular.module('panel.module', [])

.directive('panel', function() {
	var directive = {
		restrict: 'E',
		transclude: true,
		template: '<div class="custom-panel" ng-transclude></div>'
	};
	return directive;
})

.directive('panelHeader', function() {
	var directive = {
		restrict: 'E',
		transclude: true,
		scope: {
			headerTitle: '@'
		},
		controller: function($scope, $element, $attrs) {
			$scope.title = $attrs.headerTitle;
		},
		template: '<div class="custome-panel-header">' +
				'<h1 class="text-center">{{title}}</h1>'+
				'<div ng-transclude></div>'+
			'</div>'
	};
	
	return directive;
})

.directive('panelContent', function() {
	var directive = {
		restrict: 'E',
		transclude: true,
		scope: {
			contentTitle: '@'
		},
		controller: function($scope, $element, $attrs) {
			$scope.title = $attrs.contentTitle;
		},
		template: '<div class="custome-panel-content">'+
				'<h3 class="text-center">{{title}}</h3>'+
				'<div ng-transclude></div>'+
			'</div>'
	};

	return directive;
});