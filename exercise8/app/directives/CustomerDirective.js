var app = angular.module('App');

app.directive('myCustomer', function() {
	
	var directive = {
		templateUrl: 'app/directives/customerTemplate.html'
	};
	
	return directive;
});