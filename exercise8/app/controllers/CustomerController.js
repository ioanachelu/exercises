var app = angular.module('App');

app.controller('CustomerController', function($scope, customer, productService, productFactory) {
	$scope.customer = {};
	
	$scope.customers = [];

	$scope.createCustomer = function() {
		var customerObj = angular.copy($scope.customer);
		$scope.customers.push(customerObj);
		$scope.customer = {};
	};
	
});