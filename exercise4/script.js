var app = angular.module('App', []);

app.controller('CustomerController', function($scope) {
	$scope.customer = {};

	$scope.createCustomer = function() {
		console.log('Customer ' + $scope.customer.name + ' was submitted!');
	};
});